package transformation

import (
	"strings"
)

func removeRearParan(ts string) []string {
	s := strings.Trim(ts, " ")
	// splits at end of transform function, close paran rotate(32)<-
	// output: [rotate(32], etc...n
	//
	return strings.Split(s, ")")
}

func removeFrontParan(ts []string) (m map[string]string) {
	m = make(map[string]string)
	for _, v := range ts {
		if v == "" {
			continue
		}
		tmp := strings.Split(string(strings.Trim(v, " ")), "(")
		m[tmp[0]] = tmp[1]
	}
	return
}
