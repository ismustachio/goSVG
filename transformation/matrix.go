package transformation

import (
	"strconv"
	"strings"
)

type Matrix struct {
	A float64
	B float64
	C float64
	D float64
	E float64
	F float64
}

func CreateMatrices(m map[string]string) (matrix []Matrix, err error) {
	matrix = make([]Matrix, 0)
	for function, values := range m {
		switch function {
		case "rotate":
			if strings.Contains(values, ",") == false &&
				strings.Contains(values, " ") == false {
				m1, err := Rotate(values)
				if err != nil {
					return nil, err
				}
				matrix = append(matrix, m1)
			} else {
				m1, a, err := TranslateRotate(values)
				if err != nil {
					return nil, err
				}
				matrix = append(matrix, m1)
				m2, err := Rotate(a)
				if err != nil {
					return nil, err
				}
				matrix = append(matrix, m2)
				m3, a, err := TranslateRotate(values)
				if err != nil {
					return nil, err
				}
				matrix = append(matrix, m3)
			}
		case "translate":
			m, err := Translate(values)
			if err != nil {
				return nil, err
			}
			matrix = append(matrix, m)
		case "scale":
			m, err := Scale(values)
			if err != nil {
				return nil, err
			}
			matrix = append(matrix, m)
		case "skewX":
			m, err := SkewX(values)
			if err != nil {
				return nil, err
			}
			matrix = append(matrix, m)
		case "skewY":
			m, err := SkewY(values)
			if err != nil {
				return nil, err
			}
			matrix = append(matrix, m)
		case "matrix":
			m, err := MatrixFunc(values)
			if err != nil {
				return nil, err
			}
			matrix = append(matrix, m)
		}
	}
	return matrix, nil
}

func MatrixFunc(v string) (Matrix, error) {
	s := []string{}
	if strings.Contains(v, ",") {
		s = strings.Split(v, ",")
	} else {
		s = strings.Split(v, " ")
	}
	a, err := strconv.ParseFloat(s[0], 64)
	if err != nil {
		return Matrix{}, err
	}
	b, err := strconv.ParseFloat(s[1], 64)
	if err != nil {
		return Matrix{}, err
	}
	c, err := strconv.ParseFloat(s[2], 64)
	if err != nil {
		return Matrix{}, err
	}
	d, err := strconv.ParseFloat(s[3], 64)
	if err != nil {
		return Matrix{}, err
	}
	e, err := strconv.ParseFloat(s[4], 64)
	if err != nil {
		return Matrix{}, err
	}
	f, err := strconv.ParseFloat(s[5], 64)
	if err != nil {
		return Matrix{}, err
	}
	return Matrix{
		A: a,
		B: b,
		C: c,
		D: d,
		E: e,
		F: f}, nil
}
