package transformation

import (
	"strconv"
	"strings"
)

func Scale(v string) (Matrix, error) {
	sy := float64(0)
	sx := float64(0)
	err := error(nil)
	if strings.Contains(v, ",") == false { //ty = tx then
		sx, err = strconv.ParseFloat(v, 64)
		if err != nil {
			return Matrix{}, err
		}
		sy = sx
	} else {
		s := strings.Split(v, ",")
		sx, err = strconv.ParseFloat(s[0], 64)
		if err != nil {
			return Matrix{}, err
		}
		sy, err = strconv.ParseFloat(s[1], 64)
		if err != nil {
			return Matrix{}, err
		}
	}
	return Matrix{
		A: sx,
		B: 0,
		C: 0,
		D: sy,
		E: 0,
		F: 0}, nil
}
