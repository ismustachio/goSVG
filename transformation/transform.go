package transformation

func ParseTransform(ts string) (m map[string]string) {
	if ts == "" {
		return
	}
	s := removeRearParan(ts)
	m = removeFrontParan(s)
	return
}
