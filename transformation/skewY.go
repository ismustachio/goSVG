package transformation

import (
	"math"
	"strconv"
)

func SkewY(v string) (Matrix, error) {
	a, err := strconv.ParseFloat(v, 64)
	if err != nil {
		return Matrix{}, err
	}
	return Matrix{
		A: 1,
		B: math.Tan(a),
		C: 0,
		D: 1,
		E: 0,
		F: 0}, nil
}
