package transformation

import (
	"math"
	"strconv"
)

func SkewX(v string) (Matrix, error) {
	a, err := strconv.ParseFloat(v, 64)
	if err != nil {
		return Matrix{}, err
	}
	return Matrix{
		A: 1,
		B: 0,
		C: math.Tan(a),
		D: 1,
		E: 0,
		F: 0}, nil
}
