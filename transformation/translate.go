package transformation

import (
	"strconv"
	"strings"
)

func Translate(v string) (Matrix, error) {
	// check if two values were provide
	tx := float64(0)
	ty := float64(0)
	err := error(nil)
	if strings.Contains(v, ",") == false &&
		strings.Contains(v, " ") == false {
		tx, err = strconv.ParseFloat(v, 64)
		if err != nil {
			return Matrix{}, err
		}
		ty = tx
	} else if strings.Contains(v, ",") { //two values provided split by comma ","
		s := strings.Split(v, ",")
		tx, err = strconv.ParseFloat(s[0], 64)
		if err != nil {
			return Matrix{}, err
		}
		ty, err = strconv.ParseFloat(s[1], 64)
		if err != nil {
			return Matrix{}, err
		}
	} else if strings.Contains(v, " ") {
		s := strings.Split(v, " ")
		tx, err = strconv.ParseFloat(s[0], 64)
		if err != nil {
			return Matrix{}, err
		}
		ty, err = strconv.ParseFloat(s[1], 64)
		if err != nil {
			return Matrix{}, err
		}
	}
	return Matrix{
		A: 1,
		B: 0,
		C: 0,
		D: 1,
		E: tx,
		F: ty}, nil

}

func TranslateRotate(v string) (Matrix, string, error) {
	tx := float64(0)
	ty := float64(0)
	err := error(nil)
	s := []string{}
	if strings.Contains(v, " ") {
		s = strings.Split(v, " ")
		tx, err = strconv.ParseFloat(s[1], 64)
		if err != nil {
			return Matrix{}, "", err
		}
		ty, err = strconv.ParseFloat(s[2], 64)
		if err != nil {
			return Matrix{}, "", err
		}
	} else if strings.Contains(v, ",") {
		s = strings.Split(v, " ")
		tx, err = strconv.ParseFloat(s[1], 64)
		if err != nil {
			return Matrix{}, "", err
		}
		ty, err = strconv.ParseFloat(s[2], 64)
		if err != nil {
			return Matrix{}, "", err
		}
	}
	return Matrix{
		A: 1,
		B: 0,
		C: 0,
		D: 1,
		E: tx,
		F: ty}, s[0], nil
}
