package transformation

import (
	"math"
	"strconv"
)

func Rotate(v string) (Matrix, error) {
	a, err := strconv.ParseFloat(v, 64)
	if err != nil {
		return Matrix{}, err
	}
	return Matrix{
		A: math.Cos(a),
		B: math.Sin(a),
		C: -math.Sin(a),
		D: math.Cos(a),
		E: 0,
		F: 0}, nil
}
