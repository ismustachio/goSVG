package contentModel

import "gitlab.com/beardio/goSVG/attributes"

// https://www.w3.org/TR/2015/WD-SVG2-20150915/interact.html#ScriptElement
type script struct {
	ContentModel
	attributes.Attributes
}
