package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://www.w3.org/TR/2015/WD-SVG2-20150915/painting.html#MarkerElement
type marker struct {
	ContentModel
	attributes.Attributes
	RefX         string `xml:"refX,attr"`
	RefY         string `xml:"refY,attr"`
	MarkerHeight string `xml:"markerHeight,attr"`
	MarkerUnits  string `xml:"markerUnits,attr"`
	MarkerWidth  string `xml:"markerWidth,attr"`
	Orient       string `xml:"orient,attr"`
}
