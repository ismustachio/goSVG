package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFeFuncG(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/feFuncG.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, v := range m.FeComponentTransfer {
		for _, fG := range v.FeFuncG {
			if fG.Type != "identity" {
				t.Error("element do not match test file.")
			}
		}
	}
}
