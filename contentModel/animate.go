package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://svgwg.org/specs/animations/#AnimateElement
type animate struct {
	ContentModel
	attributes.Attributes
}
