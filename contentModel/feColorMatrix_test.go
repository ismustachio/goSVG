package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFeColorMatrix(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/feColorMatrix.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, v := range m.FeColorMatrix {
		if v.In != "SourceGraphic" ||
			v.Type != "matrix" ||
			v.Values != "0 0 0 0 0" {
			t.Error("Element do not match.")
		}
	}

}
