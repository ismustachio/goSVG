package contentModel

import "gitlab.com/beardio/goSVG/attributes"

// https://www.w3.org/TR/2015/WD-SVG2-20150915/styling.html#StyleElement
type style struct {
	ContentModel
	attributes.Attributes

	Target string `xml:"target,attr"`
	Media  string `xml:"media,attr"`
	Title  string `xml:"title,attr"`
}
