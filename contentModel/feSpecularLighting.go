package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://drafts.fxtf.org/filter-effects/#feSpecularLightingElement
type feSpecularLighting struct {
	ContentModel
	attributes.Attributes
	Width  string `xml:"width,attr"`
	Height string `xml:"height,attr"`
	X      string `xml:"x,attr"`
	Y      string `xml:"y,attr"`
	Result string `xml:"result,attr"`

	SurfaceScale     string `xml:"surfaceScale,attr"`
	SpecularConstant string `xml:"specularConstant,attr"`
}
