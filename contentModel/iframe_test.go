package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestIFrame(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/iframe.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, i := range m.IFrame {
		if i.SrcDoc != "did you get a cover picture yet?" {
			t.Error("element does not match test file.")
		}
	}
}
