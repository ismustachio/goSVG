package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://www.w3.org/TR/2015/WD-SVG2-20150915/text.html#TextElement
type text struct {
	ContentModel
	attributes.Attributes
	X          string `xml:"x,attr"`
	Y          string `xml:"y,attr"`
	DX         string `xml:"dx,attr"`
	DY         string `xml:"dy,attr"`
	Rotate     string `xml:"rotate,attr"`
	TextLength string `xml:"textLength,attr"`
}
