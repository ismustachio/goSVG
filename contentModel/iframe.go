package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://www.w3.org/TR/2015/WD-SVG2-20150915/embedded.html#HTMLElements
type iframe struct {
	ContentModel
	attributes.Attributes
	Width  string `xml:"width,attr"`
	Height string `xml:"height,attr"`

	SrcDoc              string `xml:"srcdoc,attr"`
	Name                string `xml:"name,attr"`
	Sandbox             string `xml:"sandbox,attr"`
	Allow               string `xml:"allow,attr"`
	AllowFullScreen     string `xml:"allowfullscreen,attr"`
	AllowPaymentRequest string `xml:"allowpaymentrequest,attr"`
	ReferrerPolicy      string `xml:"referrerpolicy,attr"`
}
