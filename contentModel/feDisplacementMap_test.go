package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFeDisplacementMap(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/feDisplacementMap.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, v := range m.FeDisplacementMap {
		if v.In2 != "turbulence" ||
			v.In != "SourceGraphic" ||
			v.Scale != "50" ||
			v.XChannelSelector != "R" ||
			v.YChannelSelector != "G" {
			t.Error("Element do not match.")
		}
	}
}
