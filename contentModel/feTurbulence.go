package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://drafts.fxtf.org/filter-effects/#feTurbulenceElement
type feTurbulence struct {
	ContentModel
	attributes.Attributes
	Width  string `xml:"width,attr"`
	Height string `xml:"height,attr"`
	X      string `xml:"x,attr"`
	Y      string `xml:"y,attr"`
	Result string `xml:"result,attr"`

	BaseFrequency string `xml:"baseFrequency,attr"`
	NumOctaves    string `xml:"numOctaves,attr"`
	Seed          string `xml:"seed,attr"`
	StitchTiles   string `xml:"stitchTiles,attr"`
}
