package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestAudio(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/audio.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, a := range m.Audio {
		if a.Src != "file.mp4" ||
			a.Controls != "controls" {
			t.Error("Element does not match")
		}
	}
}
