package contentModel

import "gitlab.com/beardio/goSVG/attributes"

// https://svgwg.org/specs/animations/#MPathElement
type mpath struct {
	ContentModel
	attributes.Attributes
}
