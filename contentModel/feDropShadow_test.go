package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFeDropShadow(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/feDropShadow.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, d := range m.Defs {
		for _, f := range d.Filter {
			for _, fD := range f.FeDropShadow {
				if fD.DX != "4" ||
					fD.DY != "8" ||
					fD.StdDeviation != "4" {
					t.Error("element do not match test file")
				}
			}
		}
	}
}
