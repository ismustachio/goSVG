package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://svgwg.org/specs/animations/#DiscardElement
type discard struct {
	ContentModel
	attributes.Attributes
}
