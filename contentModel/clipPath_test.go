package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestClipPath(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/clipPath.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, v := range m.ClipPath {
		if v.ID != "myClip" {
			t.Error("Elemnet does not match")
		}
		for _, n := range v.Circle {
			if n.CX != "40" ||
				n.CY != "35" ||
				n.R != "35" {
				t.Error("Element do not match")
			}
		}
	}

}
