package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFeOffset(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/feOffset.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, d := range m.Defs {
		for _, f := range d.Filter {
			for _, off := range f.FeOffset {
				if off.In != "SourceGraphic" ||
					off.DX != "60" ||
					off.DY != "60" {
					t.Error("element do not match test file.")
				}
			}
		}
	}
}
