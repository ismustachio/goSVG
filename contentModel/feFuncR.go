package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://drafts.fxtf.org/filter-effects/#feFuncRElement
type feFuncR struct {
	ContentModel
	attributes.Attributes
}
