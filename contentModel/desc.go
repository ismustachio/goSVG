package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://www.w3.org/TR/SVG/struct.html#DescElement
type desc struct {
	ContentModel
	attributes.Attributes
}
