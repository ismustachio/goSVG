package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestForeignObject(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/foreignObject.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, fO := range m.ForeignObject {
		if fO.X != "20" ||
			fO.Y != "20" ||
			fO.Width != "160" ||
			fO.Height != "160" {
			t.Error("element does not match test file.")
		}
	}
}
