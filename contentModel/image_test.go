package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestImage(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/image.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, i := range m.Image {
		if i.Href != "https://mdn.mozillademos.org/files/6457/mdn_logo_only_color.png" ||
			i.Height != "200" ||
			i.Width != "200" {
			t.Error("element does not match test file.")
		}
	}
}
