package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFeConvolveMatrix(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/feConvolveMatrix.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, v := range m.FeConvolveMatrix {
		if v.KernelMatrix != "3 0 0" {
			t.Error("Element does not match")
		}
	}
}
