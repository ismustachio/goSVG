package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestSymbol(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/symbol.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, s := range m.Symbol {
		if s.ID != "myDot" ||
			s.Width != "10" ||
			s.Height != "10" ||
			s.ViewBox != "0 0 2 2" {
			t.Error("element does not match test file.")
		}
		for _, c := range s.Circle {
			if c.CX != "1" {
				t.Error("element does not match test file.")
			}
		}
	}
}
