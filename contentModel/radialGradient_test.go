package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestRadialGradient(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/radialGradient.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, d := range m.Defs {
		for _, r := range d.RadialGradient {
			if r.ID != "myGradient" {
				t.Error("element does not match test file.")
			}
		}
	}
}
