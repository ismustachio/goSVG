package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://www.w3.org/TR/SVG/shapes.html#EllipseElement
type ellipse struct {
	ContentModel
	attributes.Attributes
	CX string `xml:"cx,attr"`
	CY string `xml:"cy,attr"`
	RX string `xml:"rx,attr"`
	RY string `xml:"ry,attr"`
}
