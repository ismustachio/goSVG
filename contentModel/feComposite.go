package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://drafts.fxtf.org/filter-effects/#feCompositeElement
type feComposite struct {
	ContentModel
	attributes.Attributes
	Width  string `xml:"width,attr"`
	Height string `xml:"height,attr"`
	X      string `xml:"x,attr"`
	Y      string `xml:"y,attr"`
	Result string `xml:"result,attr"`

	K1 string `xml:"k1,attr"`
	K2 string `xml:"k2,attr"`
	K3 string `xml:"k3,attr"`
	K4 string `xml:"k4,attr"`
}
