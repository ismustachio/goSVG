package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestLinearGradient(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/linearGradient.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, d := range m.Defs {
		for _, l := range d.LinearGradient {
			if l.ID != "myGradient" ||
				l.GradientTransform != "rotate(90)" {
				t.Error("element does not match test file.")
			}
		}
	}
}
