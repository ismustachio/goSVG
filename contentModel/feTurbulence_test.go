package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFeTurbulence(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/feTurbulence.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, f := range m.Filter {
		for _, fT := range f.FeTurbulence {
			if fT.Type != "turbulence" ||
				fT.BaseFrequency != "0.05" ||
				fT.NumOctaves != "2" ||
				fT.Result != "turbulence" {
				t.Error("element do not match test file.")
			}
		}
	}
}
