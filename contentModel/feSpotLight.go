package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://drafts.fxtf.org/filter-effects/#feSpotLightElement
type feSpotLight struct {
	ContentModel
	attributes.Attributes
	X                 string `xml:"x,attr"`
	Y                 string `xml:"y,attr"`
	Z                 string `xml:"z,attr"`
	PointsAtX         string `xml:"pointsAtX,attr"`
	PointsAtY         string `xml:"pointsAtY,attr"`
	PointsAtZ         string `xml:"pointsAtZ,attr"`
	LimitingConeAngle string `xml:"limitingConeAngle,attr"`
}
