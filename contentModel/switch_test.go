package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestSwitch(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/switch.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, s := range m.Switch {
		for _, tx := range s.Text {
			if tx.SystemLanguage != "ar" {
				t.Error("element does not match test file.")
			}
		}
	}
}
