package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://drafts.fxtf.org/filter-effects/#feBlendElement
type feBlend struct {
	ContentModel
	attributes.Attributes
	Width  string `xml:"width,attr"`
	Height string `xml:"height,attr"`
	X      string `xml:"x,attr"`
	Y      string `xml:"y,attr"`
	Mode   string `xml:"mode,attr"`
	Result string `xml:"result,attr"`
}
