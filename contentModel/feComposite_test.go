package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFeComposite(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/feComposite.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, v := range m.FeComposite {
		if v.In != "SourceGraphic" ||
			v.In2 != "BackgroundImage" ||
			v.Result != "comp" ||
			v.Operator != "arithmetic" ||
			v.K1 != ".5" ||
			v.K2 != ".5" ||
			v.K3 != ".5" ||
			v.K4 != ".5" {
			t.Error("Element do not match.")
		}
	}

}
