package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestAnimate(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/animate.svg")
	if err != nil {
		t.Error(err)
	}
	a := &Svg{}
	err = xml.Unmarshal([]byte(f), a)
	if err != nil {
		t.Error(err)
	}

	for _, v := range a.Rect {
		for _, i := range v.Animate {
			if i.AttributeName != "opacity" ||
				i.From != "1" ||
				i.To != "0" ||
				i.Dur != "5s" ||
				i.RepeatCount != "indefinite" {
				t.Error("Element does not match test file")
			}
		}
	}
}
