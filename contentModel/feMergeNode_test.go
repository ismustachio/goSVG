package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFeMergeNode(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/feMergeNode.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, fil := range m.Filter {
		for _, fM := range fil.FeMerge {
			for _, mN := range fM.FeMergeNode {
				if mN.In != "SourceGraphic" {
					t.Error("element do not match test file.")
				}
			}
		}
	}
}
