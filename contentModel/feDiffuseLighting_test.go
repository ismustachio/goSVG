package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFeDiffuseLighting(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/feDiffuseLighting.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, v := range m.FeDiffuseLighting {
		if v.In != "SourceGraphic" ||
			v.Result != "light" ||
			v.LightingColor != "white" {
			t.Error("Element do not match")
		}
		for _, n := range v.FePointLight {
			if n.X != "150" ||
				n.Y != "60" ||
				n.Z != "20" {
				t.Error("Element do not match")
			}
		}
	}
}
