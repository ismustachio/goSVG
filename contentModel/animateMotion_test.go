package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestAnimateMotion(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/animateMotion.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, v := range m.Path {
		for _, n := range v.AnimateMotion {
			if n.Dur != "6s" ||
				n.RepeatCount != "indefinite" ||
				n.Rotate != "auto" {
				t.Error("Element does not match")
			}
		}
	}
}
