package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://www.w3.org/TR/2015/WD-SVG2-20150915/pservers.html#RadialGradientElement
type radialGradient struct {
	ContentModel
	attributes.Attributes
	CX string `xml:"cx,attr"`
	CY string `xml:"cy,attr"`

	Fx string `xml:"fx,attr"`
	Fy string `xml:"fy,attr"`

	Fr string `xml:"fr,attr"`
	R  string `xml:"r,attr"`
}
