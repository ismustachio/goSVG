package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://drafts.fxtf.org/filter-effects/#fePointLightElement
type fePointLight struct {
	ContentModel
	attributes.Attributes
	X string `xml:"x,attr"`
	Y string `xml:"y,attr"`
	Z string `xml:"z,attr"`
}
