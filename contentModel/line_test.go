package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestLine(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/line.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, l := range m.Line {
		if l.X1 != "0" ||
			l.Y1 != "80" ||
			l.X2 != "100" ||
			l.Y2 != "20" ||
			l.Stroke != "black" {
			t.Error("element does not match test file.")
		}
	}

}
