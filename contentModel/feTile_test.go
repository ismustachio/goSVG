package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFeTile(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/feTile.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, d := range m.Defs {
		for _, f := range d.Filter {
			for _, fT := range f.FeTile {
				if fT.In != "SourceGraphic" ||
					fT.X != "50" ||
					fT.Y != "50" ||
					fT.Width != "100" ||
					fT.Height != "100" {
					t.Error("element do not match test file.")
				}
			}
		}
	}
}
