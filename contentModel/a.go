package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://www.w3.org/TR/2015/WD-SVG2-20150915/linking.html#AElement
type a struct {
	ContentModel
	attributes.Attributes

	Target         string `xml:"target,attr"`
	Download       string `xml:"download,attr"`
	Ping           string `xml:"ping,attr"`
	Rel            string `xml:"rel,attr"`
	HrefLang       string `xml:"hreflang,attr"`
	ReferrerPolicy string `xml:"referrerPolicy,attr"`
}
