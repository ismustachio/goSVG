package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://drafts.fxtf.org/filter-effects/#elementdef-femergenode
type feMergeNode struct {
	ContentModel
	attributes.Attributes
}
