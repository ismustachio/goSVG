package contentModel

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"testing"
)

func TestSvg(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/svg.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	fmt.Println(m.XMLName)
	for _, s := range m.SvgE {
		if s.ViewBox != "0 0 10 10" ||
			s.X != "200" ||
			s.Width != "100" {
			t.Error("element does not match test file.")
		}
		for _, c := range s.Circle {
			if c.CX != "5" {
				t.Error("element does not match test file.")
			}
		}
	}
}
