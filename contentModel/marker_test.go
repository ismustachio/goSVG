package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestMarker(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/marker.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, d := range m.Defs {
		for _, m := range d.Marker {
			if m.ID != "arrow" ||
				m.ViewBox != "0 0 10 10" ||
				m.RefX != "5" ||
				m.RefY != "5" ||
				m.MarkerWidth != "6" ||
				m.MarkerHeight != "6" ||
				m.Orient != "auto-start-reverse" {
				t.Error("element does not match test file.")
			}
		}
	}
}
