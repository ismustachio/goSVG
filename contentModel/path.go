package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://www.w3.org/TR/2015/WD-SVG2-20150915/paths.html#PathElement
type path struct {
	ContentModel
	attributes.Attributes
	D          string `xml:"d,attr"`
	PathLength string `xml:"pathLength,attr"`
}
