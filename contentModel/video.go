package contentModel

// https://html.spec.whatwg.org/multipage/media.html#the-video-element
type video struct {
	Poster      string `xml:"poster,attr"`
	Preload     string `xml:"preload,attr"`
	AutoPlay    string `xml:"autoplay,attr"`
	PlaysInLine string `xml:"playsinline,attr"`
	Loop        string `xml:"loop,attr"`
	Muted       string `xml:"muted,attr"`
	Controls    string `xml:"controls,attr"`
	Width       string `xml:"width,attr"`
	Height      string `xml:"height,attr"`
}
