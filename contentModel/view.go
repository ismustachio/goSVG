package contentModel

import "gitlab.com/beardio/goSVG/attributes"

// https://www.w3.org/TR/2015/WD-SVG2-20150915/linking.html#ViewElement
type view struct {
	ContentModel
	attributes.Attributes
}
