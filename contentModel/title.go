package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://www.w3.org/TR/2015/WD-SVG2-20150915/struct.html#TitleElement
type title struct {
	ContentModel
	attributes.Attributes
}
