package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://www.w3.org/TR/2015/WD-SVG2-20150915/embedded.html#ForeignObjectElement
type foreignObject struct {
	ContentModel
	attributes.Attributes
	X      string `xml:"x,attr"`
	Y      string `xml:"y,attr"`
	Width  string `xml:"width,attr"`
	Height string `xml:"height,attr"`
}
