package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestG(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/g.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, g := range m.G {
		if g.Fill != "white" ||
			g.Stroke != "green" ||
			g.StrokeWidth != "5" {
			t.Error("element does not match test file.")
		}
		for _, c := range g.Circle {
			if c.CX != "40" {
				t.Error("element does not match test file.")
			}
		}
	}
}
