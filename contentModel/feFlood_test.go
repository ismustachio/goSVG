package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFeFlood(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/feFlood.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, d := range m.Defs {
		for _, f := range d.Filter {
			for _, fF := range f.FeFlood {
				if fF.X != "50" ||
					fF.Y != "50" ||
					fF.Width != "100" ||
					fF.Height != "100" ||
					fF.FloodColor != "green" ||
					fF.FloodOpacity != "0.5" {
					t.Error("element does not match the test file.")
				}
			}
		}
	}

}
