package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFeSpecularLighting(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/feSpecularLighting.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, d := range m.Defs {
		for _, f := range d.Filter {
			for _, fS := range f.FeSpecularLighting {
				if fS.Result != "spotlight" ||
					fS.SpecularConstant != "1.5" ||
					fS.SpecularExponent != "80" ||
					fS.LightingColor != "#FFF" {
					t.Error("element do not match test file.")
				}
			}
		}
	}
}
