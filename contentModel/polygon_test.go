package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestPolygon(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/polygon.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, p := range m.Polygon {
		if p.Points != "0,100 50,25 50,75 100,0" {
			t.Error("element does not match test file.")
		}
	}

}
