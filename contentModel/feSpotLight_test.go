package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFeSpotLight(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/feSpotLight.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, d := range m.Defs {
		for _, f := range d.Filter {
			for _, fS := range f.FeSpecularLighting {
				for _, spot := range fS.FeSpotLight {
					if spot.X != "600" ||
						spot.Y != "600" ||
						spot.Z != "400" ||
						spot.LimitingConeAngle != "5.5" {
						t.Error("element do not match test file.")
					}
				}
			}
		}
	}
}
