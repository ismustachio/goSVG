package contentModel

import "gitlab.com/beardio/goSVG/attributes"

// https://drafts.fxtf.org/css-masking-1/#ClipPathElement
type clipPath struct {
	ContentModel
	attributes.Attributes
}
