package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestMPath(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/mpath.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, p := range m.Path {
		for _, aM := range p.AnimateMotion {
			for _, mP := range aM.MPath {
				if mP.Href != "#path1" {
					t.Error("element does not match test file.")
				}
			}
		}
	}
}
