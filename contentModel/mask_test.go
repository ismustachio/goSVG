package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestMask(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/mask.svg")
	if err != nil {
		t.Error(err)
	}
	s := &Svg{}
	err = xml.Unmarshal([]byte(f), s)
	if err != nil {
		t.Error(err)
	}
	for _, m := range s.Mask {
		if m.ID != "myMask" {
			t.Error("element does not match.")
		}
		for _, r := range s.Rect {
			if r.X != "0" {
				t.Error("element does not match.")
			}
		}
	}
}
