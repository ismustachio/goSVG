package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFeImage(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/feImage.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, v := range m.Defs {
		for _, f := range v.Filter {
			for _, fI := range f.FeImage {
				if fI.Href != "/files/6457/mdn_logo_only_color.png" {
					t.Error("element do not match test file.")

				}
			}
		}
	}
}
