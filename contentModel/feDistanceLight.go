package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://drafts.fxtf.org/filter-effects/#feDistantLightElement
type feDistanceLight struct {
	ContentModel
	attributes.Attributes

	Azimuth   string `xml:"azimuth,attr"`
	Elevation string `xml:"elevation,attr"`
}
