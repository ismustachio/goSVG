package contentModel

import "gitlab.com/beardio/goSVG/attributes"

// https://www.w3.org/TR/2015/WD-SVG2-20150915/pservers.html#StopElement
type stop struct {
	ContentModel
	attributes.Attributes

	Offset string `xml:"offset,attr"`
}
