package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://drafts.fxtf.org/filter-effects/#feColorMatrixElement
type feColorMatrix struct {
	ContentModel
	attributes.Attributes
	Width  string `xml:"width,attr"`
	Height string `xml:"height,attr"`
	X      string `xml:"x,attr"`
	Values string `xml:"values,attr"`
	Result string `xml:"result,attr"`
}
