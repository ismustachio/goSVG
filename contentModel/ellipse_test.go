package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestEllipse(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/ellipse.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, v := range m.Ellipse {
		if v.CX != "100" ||
			v.CY != "214" ||
			v.RX != "110" ||
			v.RY != "50" {
			t.Error("Element do not match")
		}
	}
}
