package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFeGaussianBlur(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/feGaussianBlur.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, v := range m.FeGaussianBlur {
		for _, f := range v.Filter {
			for _, fG := range f.FeGaussianBlur {
				if fG.In != "SourceGraphic" ||
					fG.StdDeviation != "5" {
					t.Error("element do not match test file.")
				}
			}
		}
	}
}
