package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFilter(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/filter.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, f := range m.Filter {
		if f.ID != "displacementFilter" {
			t.Error("element do not match test file.")
		}
	}
}
