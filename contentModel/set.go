package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://svgwg.org/specs/animations/#SetElement
type set struct {
	ContentModel
	attributes.Attributes

	To string `xml:"to"`
}
