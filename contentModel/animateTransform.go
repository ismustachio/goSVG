package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://svgwg.org/specs/animations/#AnimateTransformElement
type animateTransform struct {
	ContentModel
	attributes.Attributes
}
