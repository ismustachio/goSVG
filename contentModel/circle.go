package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
	"gitlab.com/beardio/goSVG/transformation"
	"log"
	"math"
	"strconv"
)

// https://www.w3.org/TR/SVG/shapes.html#CircleElement
type circle struct {
	ContentModel
	attributes.Attributes

	CX string `xml:"cx,attr"`
	CY string `xml:"cy,attr"`
	R  string `xml:"r,attr"`

	//transformed geometry properties
	newCX float64
	newCY float64
	newR  float64
}

func (c circle) TransformFunc() {
	m := transformation.ParseTransform(c.Transform)
	matrix, err := transformation.CreateMatrices(m)
	if err != nil {
		log.Fatal(err)
	}
	c.applyCircleTransform(matrix)
}

func (c circle) applyCircleTransform(m []transformation.Matrix) {
	oldX, err := strconv.ParseFloat(c.CX, 64)
	if err != nil {

	}
	oldY, err := strconv.ParseFloat(c.CY, 64)
	if err != nil {

	}
	oldR, err := strconv.ParseFloat(c.R, 64)
	if err != nil {

	}
	//combine the matrices
	matrix := transformation.Matrix{}
	for i, v := range m {
		if i == 0 {
			matrix = v
			continue
		}
		matrix = transformation.Matrix{A: matrix.A*v.A + matrix.C*v.B + matrix.E,
			B: matrix.B*v.A + matrix.D*v.B + matrix.F,
			C: matrix.A*v.C + matrix.C*v.D + matrix.E,
			D: matrix.B*v.C + matrix.D*v.D + matrix.F,
			E: matrix.A*v.E + matrix.C*v.F + matrix.E,
			F: matrix.B*v.E + matrix.D*v.F + matrix.F,
		}

	}
	ncx := matrix.A*oldX + matrix.C*oldY + matrix.E
	ncy := matrix.B*oldX + matrix.D*oldY + matrix.F

	//just get an edge then figure out the new radius
	oldX = oldX + oldR
	eX := matrix.A*oldX + matrix.C*oldY + matrix.E
	eY := matrix.B*oldX + matrix.D*oldY + matrix.F

	c.newR = math.Max(math.Abs(ncx-eX), math.Abs(ncy-eY))
	c.newCX = ncx
	c.newCY = ncy
}
