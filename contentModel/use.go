package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://www.w3.org/TR/2015/WD-SVG2-20150915/struct.html#UseElement
type use struct {
	ContentModel
	attributes.Attributes
	X      string `xml:"x,attr"`
	Y      string `xml:"y,attr"`
	Width  string `xml:"width,attr"`
	Height string `xml:"height,attr"`
}
