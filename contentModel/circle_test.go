package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestCircle(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/circle.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, v := range m.Circle {
		if v.R != "60" ||
			v.CY != "97" ||
			v.CX != "497" ||
			v.StrokeWidth != "2" ||
			v.Stroke != "#E4AF4C" ||
			v.Fill != "red" {
			t.Error("Element do not match.")
		}
	}

}

func TestCircleTransform(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/circle.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, c := range m.Circle {
		c.TransformFunc()
	}
}
