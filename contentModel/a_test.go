package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
	//	"fmt"
)

func TestA(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/a.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	//TODO: create struct literal of a test file

	href := "https://developer.mozilla.org/docs/Web/SVG/Element/circle"
	for _, v := range m.A {
		if v.Href != href {
			t.Error("Element does not match test file")
		}
		for _, n := range v.Ellipse {
			if n.CX != "2.5" ||
				n.CY != "1.5" ||
				n.RX != "2" ||
				n.RY != "1" ||
				n.Fill != "red" {
				t.Error("Element does not match test file")
			}
		}
	}
}
