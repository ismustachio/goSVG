package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://drafts.fxtf.org/filter-effects/#elementdef-fefuncg
type feFuncG struct {
	ContentModel
	attributes.Attributes
}
