package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://www.w3.org/TR/SVG/embedded.html#HTMLElements
type canvas struct {
	attributes.Attributes
	Width  string `xml:"width,attr"`
	Height string `xml:"height,attr"`
}
