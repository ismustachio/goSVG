package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestStop(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/stop.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, d := range m.Defs {
		for _, l := range d.LinearGradient {
			for _, s := range l.Stop {
				if s.Offset != "5%" ||
					s.StopColor != "gold" {
					t.Error("element does not match test file.")
				}
			}
		}
	}
}
