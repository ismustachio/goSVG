package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestDiscard(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/discard.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, v := range m.Ellipse {
		for _, d := range v.Discard {
			if d.Begin != "2s" {
				t.Error("Element does not match")
			}
		}
	}
}
