package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://drafts.fxtf.org/filter-effects/#feFuncBElement
type feFuncB struct {
	ContentModel
	attributes.Attributes
}
