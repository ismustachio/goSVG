package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://drafts.fxtf.org/css-masking-1/#MaskElement
type mask struct {
	ContentModel
	attributes.Attributes
	X      string `xml:"x,attr"`
	Y      string `xml:"y,attr"`
	Width  string `xml:"width,attr"`
	Height string `xml:"height,attr"`

	MaskUnits        string `xml:"maskUnits,attr"`
	MaskContentUnits string `xml:"maskContentUnits,attr"`
}
