package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestAnimateTransform(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/animateMotion.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, v := range m.Rect {
		for _, n := range v.AnimateTransform {
			if n.AttributeName != "transform" ||
				n.Type != "rotate" ||
				n.From != "0" ||
				n.To != "90" ||
				n.Dur != "5s" ||
				n.Additive != "replace" ||
				n.Fill != "freeze" {
				t.Error("Element does not match")
			}
		}
	}

}
