package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestText(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/text.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, tx := range m.Text {
		if tx.X != "20" ||
			tx.Y != "35" ||
			tx.Class != "small" {
			t.Error("element does not match test file.")
		}
	}

}
