package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://drafts.fxtf.org/filter-effects/#feFuncAElement
type feFuncA struct {
	ContentModel
	attributes.Attributes
}
