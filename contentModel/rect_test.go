package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestRect(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/rect.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, r := range m.Rect {
		if r.X != "120" ||
			r.Y != "0" ||
			r.Width != "100" ||
			r.Height != "100" ||
			r.RX != "15" ||
			r.RY != "15" {
			t.Error("element does not match test file.")
		}
	}
}
