package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestStyle(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/style.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
}
