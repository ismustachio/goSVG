package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFeFuncR(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/feFuncR.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, v := range m.FeComponentTransfer {
		for _, fR := range v.FeFuncG {
			if fR.Type != "identity" {
				t.Error("element do not match test file.")
			}
		}
	}

}
