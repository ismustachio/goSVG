package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://www.w3.org/TR/2015/WD-SVG2-20150915/shapes.html#PolylineElement
type polyline struct {
	ContentModel
	attributes.Attributes
	Points string `xml:"points,attr"`
}
