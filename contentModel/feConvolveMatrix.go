package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

//https://drafts.fxtf.org/filter-effects/#feConvolveMatrixElement
type feConvolveMatrix struct {
	ContentModel
	attributes.Attributes
	Width  string `xml:"width,attr"`
	Height string `xml:"height,attr"`
	X      string `xml:"x,attr"`
	Y      string `xml:"y,attr"`
	Result string `xml:"result,attr"`

	Order         string `xml:"order,attr"`
	KernelMatrix  string `xml:"kernelMatrix,attr"`
	Divisor       string `xml:"divisor,attr"`
	Bias          string `xml:"bias,attr"`
	TargetX       string `xml:"targetX,attr"`
	TargetY       string `xml:"targetY,attr"`
	PreserveAlpha string `xml:"preserveAlpha,attr"`
}
