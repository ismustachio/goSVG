package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://drafts.fxtf.org/filter-effects/#FilterElement
type filter struct {
	ContentModel
	attributes.Attributes
	Width  string `xml:"width,attr"`
	Height string `xml:"height,attr"`
	X      string `xml:"x,attr"`
	Y      string `xml:"y,attr"`

	FilterUnits    string `xml:"filterUnits,attr"`
	PrimitiveUnits string `xml:"primitiveUnits,attr"`
}
