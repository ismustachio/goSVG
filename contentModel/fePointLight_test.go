package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFePointLight(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/fePointLight.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, d := range m.Defs {
		for _, f := range d.Filter {
			for _, fS := range f.FeSpecularLighting {
				for _, fP := range fS.FePointLight {
					if fP.X != "50" ||
						fP.Z != "220" {
						t.Error("element do not match test file.")
					}
				}
			}
		}
	}
}
