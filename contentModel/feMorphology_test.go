package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFeMorphology(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/feMorphology.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, f := range m.Filter {
		for _, fM := range f.FeMorphology {
			if fM.Operator != "erode" ||
				fM.Radius != "1" {
				t.Error("element do not match test file.")
			}
		}
	}
}
