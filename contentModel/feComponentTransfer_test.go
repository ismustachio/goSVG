package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFeComponentTransfer(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/feComponentTransfer.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, v := range m.FeComponentTransfer {
		for _, r := range v.FeFuncR {
			if r.Type != "identity" {
				t.Error("Invalid element")
			}
		}
		for _, g := range v.FeFuncG {
			if g.Type != "identity" {
				t.Error("Invalid element")
			}
		}
		for _, b := range v.FeFuncB {
			if b.Type != "identity" {
				t.Error("Invalid element")
			}
		}
		for _, a := range v.FeFuncA {
			if a.Type != "identity" {
				t.Error("Invalid element")
			}
		}
	}

}
