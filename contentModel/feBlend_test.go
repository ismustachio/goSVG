package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFeBlend(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/feBlend.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, v := range m.FeBlend {
		if v.In != "SourceGraphic" ||
			v.In2 != "floodFill" ||
			v.Mode != "multiply" {
			t.Error("Element do not match")
		}
	}
}
