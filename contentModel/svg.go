package contentModel

import (
	"encoding/xml"
	"gitlab.com/beardio/goSVG/attributes"
)

// https://www.w3.org/TR/SVG2/struct.html#SVGElement
type Svg struct {
	XMLName xml.Name `xml:"svg"`
	ContentModel
	attributes.Attributes
	X      string `xml:"x,attr"`
	Y      string `xml:"y,attr"`
	Width  string `xml:"width,attr"`
	Height string `xml:"height,attr"`
}

func (s Svg) ApplyTransformation() {
	//loop thru contentModel/elements of all the svg in the viewport
	for _, e := range s.Circle {
		e.TransformFunc()
	}
}
