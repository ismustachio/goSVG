package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestTextPath(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/textPath.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, tx := range m.Text {
		for _, tp := range tx.TextPath {
			if tp.Href != "#MyPath" {
				t.Error("element does not match test file.")
			}
		}
	}
}
