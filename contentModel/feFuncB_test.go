package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFeFuncB(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/feFuncB.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, v := range m.FeComponentTransfer {
		for _, fB := range v.FeFuncB {
			if fB.Type != "identity" {
				t.Error("element do not match test file.")
			}
		}
	}

}
