package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://svgwg.org/specs/animations/#AnimateMotionElement
type animateMotion struct {
	ContentModel
	attributes.Attributes

	Path      string `xml:"path,attr"`
	KeyPoints string `xml:"keyPoints,attr"`
	Rotate    string `xml:"rotate,attr"`
	Origin    string `xml:"origin,attr"`
}
