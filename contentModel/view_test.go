package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestView(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/view.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, v := range m.View {
		if v.ID != "halfSizeView" ||
			v.ViewBox != "0 0 1200 400" {
			t.Error("element does not match test file.")
		}
	}
}
