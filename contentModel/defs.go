package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://www.w3.org/TR/2015/WD-SVG2-20150915/struct.html#DefsElement
type defs struct {
	ContentModel
	attributes.Attributes
}
