package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestPath(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/path.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, p := range m.Path {
		if p.D != `M 10,30
           A 20,20 0,0,1 50,30
           A 20,20 0,0,1 90,30
           Q 90,60 50,90
           Q 10,60 10,30 z` {
			t.Error("element does not match test file.")
		}
	}

}
