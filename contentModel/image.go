package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://www.w3.org/TR/2015/WD-SVG2-20150915/embedded.html#ImageElement
type image struct {
	ContentModel
	attributes.Attributes
	X string `xml:"x,attr"`
	Y string `xml:"y,attr"`
}
