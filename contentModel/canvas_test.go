package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestCanvas(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/audio.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, v := range m.Canvas {
		if v.Width != "300" ||
			v.Height != "150" {
			t.Error("Element do not match")
		}
	}

}
