package contentModel

import "gitlab.com/beardio/goSVG/attributes"

// https://www.w3.org/TR/SVG/embedded.html#HTMLElements
type audio struct {
	attributes.Attributes
	Preload  string `xml:"preload,attr"`
	AutoPlay string `xml:"autoplay,attr"`
	Loop     string `xml:"loop,attr"`
	Muted    string `xml:"muted,attr"`
	Controls string `xml:"controls,attr"`
}
