package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFeFuncA(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/feFuncA.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, v := range m.FeComponentTransfer {
		for _, fA := range v.FeFuncA {
			if fA.Type != "identity" {
				t.Error("element do not match test file.")
			}
		}
	}

}
