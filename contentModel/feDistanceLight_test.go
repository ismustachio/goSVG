package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestFeDistanceLight(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/feDistanceLight.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, v := range m.FeDistanceLight {
		if v.Azimuth != "45" ||
			v.Elevation != "45" {
			t.Error("Element do not match test file.")
		}
	}

}
