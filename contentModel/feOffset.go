package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://drafts.fxtf.org/filter-effects/#feOffsetElement
type feOffset struct {
	ContentModel
	attributes.Attributes
	Width  string `xml:"width,attr"`
	Height string `xml:"height,attr"`
	X      string `xml:"x,attr"`
	Y      string `xml:"y,attr"`
	Result string `xml:"result,attr"`
	DX     string `xml:"dx,attr"`
	DY     string `xml:"dy,attr"`
}
