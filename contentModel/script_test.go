package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestScript(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/script.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, s := range m.Script {
		if s.Type != "text/javascript" {
			t.Error("element does not match test file.")
		}
	}
}
