package contentModel

import (
	"gitlab.com/beardio/goSVG/attributes"
)

// https://drafts.fxtf.org/filter-effects/#feDisplacementMapElement
type feDisplacementMap struct {
	ContentModel
	attributes.Attributes
	Width  string `xml:"width,attr"`
	Height string `xml:"height,attr"`
	X      string `xml:"x,attr"`
	Y      string `xml:"y,attr"`
	Result string `xml:"result,attr"`

	Scale            string `xml:"scale,attr"`
	XChannelSelector string `xml:"xChannelSelector,attr"`
	YChannelSelector string `xml:"yChannelSelector,attr"`
}
