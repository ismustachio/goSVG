package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestPattern(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/pattern.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, d := range m.Defs {
		for _, p := range d.Pattern {
			if p.ID != "star" ||
				p.ViewBox != "0,0,10,10" ||
				p.Width != "10%" ||
				p.Height != "10%" {
				t.Error("element does not match test file.")
			}
		}
	}
}
