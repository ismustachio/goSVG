package contentModel

import "gitlab.com/beardio/goSVG/attributes"

// https://www.w3.org/TR/2015/WD-SVG2-20150915/text.html#TextPathElement
type textPath struct {
	ContentModel
	attributes.Attributes

	LengthAdjust string `xml:"lengthAdjust,attr"`
	Method       string `xml:"method,attr"`
	Path         string `xml:"path,attr"`
	Side         string `xml:"side,attr"`
	Spacing      string `xml:"spacing,attr"`
	StartOffset  string `xml:"startOffset,attr"`
	TextLength   string `xml:"textLength,attr"`
}
