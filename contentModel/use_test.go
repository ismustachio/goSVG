package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestUse(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/use.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}
	for _, u := range m.Use {
		if u.Href != "#myCircle" ||
			u.X != "10" ||
			u.Fill != "blue" {
			t.Error("element do not match test file.")
		}
	}
}
