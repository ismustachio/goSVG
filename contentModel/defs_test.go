package contentModel

import (
	"encoding/xml"
	"io/ioutil"
	"testing"
)

func TestDefs(t *testing.T) {
	f, err := ioutil.ReadFile("../testdata/clipPath.svg")
	if err != nil {
		t.Error(err)
	}
	m := &Svg{}
	err = xml.Unmarshal([]byte(f), m)
	if err != nil {
		t.Error(err)
	}

	for _, v := range m.Defs {
		for _, c := range v.Circle {
			if c.ID != "myCircle" ||
				c.CX != "0" ||
				c.CY != "0" ||
				c.R != "5" {
				t.Error("Element does not match")
			}
			for _, lG := range v.LinearGradient {
				if lG.ID != "myGradient" ||
					lG.GradientTransform != "rotate(90)" {
					t.Error("Element does not match")
				}
			}
		}
	}

}
