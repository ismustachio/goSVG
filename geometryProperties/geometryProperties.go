package geometryProperties

// https://www.w3.org/TR/2015/WD-SVG2-20150915/attindex.html
type GeometryProperties struct {
	CX           string `xml:"cx,attr"`
	CY           string `xml:"cy,attr"`
	D            string `xml:"d,attr"`
	DX           string `xml:"dx,attr"`
	DY           string `xml:"dy,attr"`
	Fx           string `xml:"fx,attr"`
	Fy           string `xml:"fy,attr"`
	Fr           string `xml:"fr,attr"`
	R            string `xml:"r,attr"`
	RX           string `xml:"rx,attr"`
	RY           string `xml:"ry,attr"`
	X            string `xml:"x,attr"`
	Y            string `xml:"y,attr"`
	Z            string `xml:"z,attr"`
	Width        string `xml:"width,attr"`
	Height       string `xml:"height,attr"`
	X1           string `xml:"x1,attr"`
	Y1           string `xml:"y1,attr"`
	X2           string `xml:"x2,attr"`
	Y2           string `xml:"y2,attr"`
	PathLength   string `xml:"pathLength,attr"`
	Points       string `xml:"points,attr"`
	Result       string `xml:"result,attr"`
	Rotate       string `xml:"rotate,attr"`
	LengthAdjust string `xml:"lengthAdjust,attr"`
	TextLength   string `xml:"textLength,attr"`
	RefX         string `xml:"refX,attr"`
	RefY         string `xml:"refY,attr"`
}
