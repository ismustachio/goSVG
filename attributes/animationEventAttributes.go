package attributes

// https://svgwg.org/specs/animations/#TermAnimationEventAttribute
type AnimationEventAttributes struct {
	OnBegin  string `xml:"onbegin,attr"`
	OnEnd    string `xml:"onend,attr"`
	OnRepeat string `xml:"onrepeat,attr"`
}
