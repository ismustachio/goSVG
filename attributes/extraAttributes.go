package attributes

type ExtraAttributes struct {
	ViewBox                   string `xml:"viewBox,attr"`
	PreserveAspectRatio       string `xml:"PreserveAspectRatio,attr"`
	Transform                 string `xml:"transform,attr"`
	Additive                  string `xml:"additive,attr"`
	Accumulate                string `xml:"accumulate,attr"`
	AttributeName             string `xml:"attributeName,attr"`
	Fill                      string `xml:"fill,attr"`
	ExternalResourcesRequired string `xml:"externalResourcesRequired,attr"`
	OnUnLoad                  string `xml:"onunload,attr"`
	OnAbort                   string `xml:"onabort,attr"`
	Href                      string `xml:"href,attr"`
	Type                      string `xml:"type,attr"`
	TableValues               string `xml:"tableValues,attr"`
	Slope                     string `xml:"slope,attr"`
	Intercept                 string `xml:"intercept,attr"`
	Amplitude                 string `xml:"amplitude,attr"`
	Exponent                  string `xml:"exponent,attr"`
	Offset                    string `xml:"offset,attr"`
	In                        string `xml:"in,attr"`
	In2                       string `xml:"in2,attr"`
	KernelUnitLength          string `xml:"kernelUnitLength,attr"`
	StdDeviation              string `xml:"stdDeviation,attr"`
	EdgeMode                  string `xml:"edgeMode,attr"`
	CrossOrigin               string `xml:"crossorigin,attr"`
	Operator                  string `xml:"operator,attr"`
	SpecularExponent          string `xml:"specularExponent,attr"`
	Src                       string `xml:"src,attr"`
	GradientUnits             string `xml:"gradientUnits,attr"`
	GradientTransform         string `xml:"gradientTransform,attr"`
	SpreadMethod              string `xml:"spreadMethod,attr"`
	ZoomAndPan                string `xml:"zoomAndPan,attr"`
}
