package attributes

type AnimationValueAttributes struct {
	CalcMode   string `xml:"calcMode,attr"`
	Values     string `xml:"values,attr"`
	KeyTimes   string `xml:"keyTimes,attr"`
	KeySplines string `xml:"keySplines,attr"`
	From       string `xml:"from,attr"`
	To         string `xml:"to,attr"`
	By         string `xml:"by,attr"`
}
