package attributes

type DocumentElementEventAttributes struct {
	OnCopy  string `xml:"oncopy,attr"`
	OnCut   string `xml:"oncut,attr"`
	OnPaste string `xml:"onpaste,attr"`
}
