package attributes

type AnimationAttrTargetAttributes struct {
	AttributeName string `xml:"attributeName,attr"`
}
