package attributes

type GlobalEventAttributes struct {
	OnCancel         string `xml:"oncancel,attr"`
	OnCanPlay        string `xml:"oncanplay,attr"`
	OnCanPlayThrough string `xml:"oncanplaythrough,attr"`
	OnChange         string `xml:"onchange,attr"`
	OnClick          string `xml:"onclick,attr"`
	OnClose          string `xml:"onclose,attr"`
	OnCueChange      string `xml:"oncuechange,attr"`
	OndBlClick       string `xml:"ondblclick,attr"`
	OnDrag           string `xml:"ondrag,attr"`
	OnDragEnd        string `xml:"ondragend,attr"`
	OnDragCenter     string `xml:"ondragcenter,attr"`
	OnDragExit       string `xml:"ondragexit,attr"`
	OnDragLeave      string `xml:"ondragleave,attr"`
	OnDragOver       string `xml:"ondragover,attr"`
	OnDragStart      string `xml:"ondragstart,attr"`
	OnDrop           string `xml:"ondrop,attr"`
	OnDurationChange string `xml:"ondurationchange,attr"`
	OnEmptied        string `xml:"onemptied,attr"`
	OnEnded          string `xml:"onended,attr"`
	OnError          string `xml:"onerror"`
	OnFocus          string `xml:"onfocus,attr"`
	OnInput          string `xml:"oninput,attr"`
	OnInvalid        string `xml:"oninvalid,attr"`
	OnKeyDown        string `xml:"onkeydown,attr"`
	OnKeyPress       string `xml:"onkeypress,attr"`
	OnKeyUp          string `xml:"onkeyup,attr"`
	OnLoad           string `xml:"onload,attr"`
	OnLoadedData     string `xml:"onloadeddata,attr"`
	OnLoadedMetadata string `xml:"onloadedmetadata,attr"`
	OnLoadStart      string `xml:"onloadstart,attr"`
	OnMouseDown      string `xml:"onmousedown,attr"`
	OnMouseEnter     string `xml:"onmouseenter,attr"`
	OnMouseLeave     string `xml:"onmouseleave,attr"`
	OnMouseMove      string `xml:"onmousemove,attr"`
	OnMouseOut       string `xml:"onmouseout,attr"`
	OnMouseOver      string `xml:"onmouseover,attr"`
	OnMouseUp        string `xml:"onmouseup,attr"`
	OnMouseWheel     string `xml:"onmousewheel,attr"`
	OnPause          string `xml:"onpause,attr"`
	OnPlay           string `xml:"onplay,attr"`
	OnPlaying        string `xml:"onplaying,attr"`
	OnProgress       string `xml:"onprogress,attr"`
	OnRateChange     string `xml:"onratechange,attr"`
	OnReset          string `xml:"onreset,attr"`
	OnResize         string `xml:"onresize"`
	OnScroll         string `xml:"onscroll"`
	OnSeeked         string `xml:"onseeked,attr"`
	OnSeeking        string `xml:"onseeking,attr"`
	OnSelect         string `xml:"onselect,attr"`
	OnShow           string `xml:"onshow,attr"`
	OnStalled        string `xml:"onstalled,attr"`
	OnSubmit         string `xml:"onsubmit,attr"`
	OnSuspend        string `xml:"onsuspend,attr"`
	OnTimeUpdate     string `xml:"ontimeupdate,attr"`
	OnToggle         string `xml:"ontoggle,attr"`
	OnVolumeChange   string `xml:"onvolumechange,attr"`
	OnWaiting        string `xml:"onwaiting,attr"`
}
