package attributes

type ConditionalProcessAttributes struct {
	RequiredExtensions string `xml:"requiredExtensions,attr"`
	SystemLanguage     string `xml:"systemLanguage,attr"`
}
