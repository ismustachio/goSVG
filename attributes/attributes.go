package attributes

// https://www.w3.org/TR/2015/WD-SVG2-20150915/attindex.html
type Attributes struct {
	AnimationEventAttributes
	AnimationTimingAttributes
	AnimationValueAttributes
	AriaAttributes
	ExtraAttributes
	ConditionalProcessAttributes
	CoreAttributes
	DocumentElementEventAttributes
	GlobalEventAttributes
	GraphicalEventAttributes
	PresentationAttributes
	GlobalHtmlAttributes
}
