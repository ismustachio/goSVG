package attributes

type AnimationTimingAttributes struct {
	Begin       string `xml:"begin,attr"`
	Dur         string `xml:"dur,attr"`
	End         string `xml:"end,attr"`
	Min         string `xml:"min,attr"`
	Max         string `xml:"max,attr"`
	Restart     string `xml:"restart,attr"`
	RepeatCount string `xml:"repeatCount,attr"`
	RepeatDur   string `xml:"repeatDur,attr"`
}
