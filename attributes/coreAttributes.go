package attributes

type CoreAttributes struct {
	ID       string `xml:"id,attr"`
	TabIndex string `xml:"tabindex,attr"`
	Lang     string `xml:"lang,attr"`
	Class    string `xml:"class,attr"`
	XmlSpace string `xml:"xml:space,attr"`
	Style    string `xml:"style"`
	Space    string `xml:"space,attr"`
}
