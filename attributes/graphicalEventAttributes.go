package attributes

type GraphicalEventAttributes struct {
	OnFocusin  string `xml:"onfocusin,attr"`
	OnFocusOut string `xml:"onfocusout,attr"`
}
