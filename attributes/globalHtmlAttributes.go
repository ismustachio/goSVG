package attributes

type GlobalHtmlAttributes struct {
	AccessKey         string `xml:"accesskey,attr"`
	AutoCapitalize    string `xml:"autocapitalize,attr"`
	ContentedEditable string `xml:"contentededitable,attr"`
	Dir               string `xml:"dir,attr"`
	Draggable         string `xml:"draggable,attr"`
	EnterKeyHint      string `xml:"enterkeyhint,attr"`
	Hidden            string `xml:"hidden,attr"`
	InputMode         string `xml:"inputmode,attr"`
	Is                string `xml:"is,attr"`
	ItemID            string `xml:"itemid,attr"`
	ItemProp          string `xml:"itemprop,attr"`
	ItemRef           string `xml:"itemref,attr"`
	ItemScope         string `xml:"itemscope,attr"`
	ItemType          string `xml:"itemtype,attr"`
	CoreAttributes
	Nonce      string `xml:"nonce,attr"`
	SpellCheck string `xml:"spellcheck,attr"`
	Title      string `xml:"title,attr"`
	Translate  string `xml:"translate,attr"`
}
